using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
[CreateAssetMenu(fileName = "New Cube Shape", menuName = "ScriptableObjects/Cube Shape")]
public class ScriptableCubeShape : ScriptableObject
{
    public Texture2D image;
    public int pixelSamplingInterval = 2;
    public List<PositionColorPair> Pairs = new List<PositionColorPair>();

    [System.Serializable]
    public class PositionColorPair
    {
        public Vector3 Position;
        public Color Color;

        public PositionColorPair(Vector3 position, Color color)
        {
            Color = color;
            Position = position;
        }
    }

    public void GenerateCubesFromImage()
    {
        ClearCubes();

        int width = image.width;
        int height = image.height;

        for (int x = 0; x < width; x += pixelSamplingInterval)
        {
            for (int y = 0; y < height; y += pixelSamplingInterval)
            {
                Color pixelColor = image.GetPixel(x, y);

                // Skip empty or transparent pixels
                if (pixelColor.a <= 0)
                    continue;

                Vector3 position = new Vector3(x / pixelSamplingInterval, 0, y / pixelSamplingInterval); // Assuming Y-axis as the vertical axis
                Pairs.Add(new PositionColorPair(position, pixelColor));
                
            }
        }

        
    }

    public void ClearCubes()
    {
        if (Pairs.Count <= 0)
            return;

        Pairs.Clear();
    }
}
#if UNITY_EDITOR
[CustomEditor(typeof(ScriptableCubeShape))]
public class ScriptableCubeShapeEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        ScriptableCubeShape generator = (ScriptableCubeShape)target;

        if (GUILayout.Button("Generate"))
        {
            generator.GenerateCubesFromImage();
        }

        if (GUILayout.Button("Clear"))
        {
            generator.ClearCubes();
        }

    }
}
#endif