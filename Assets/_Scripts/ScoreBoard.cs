using System;
using TMPro;
using UnityEngine;

public class ScoreBoard : MonoBehaviour, IScriptableEventListener, IPlayerScoreHolder
{
    [SerializeField] ScriptableEvent onCubeCollected;
    [SerializeField] ScriptableEvent onGameStateChanged;
    [SerializeField] ScriptableEvent onNewPlayerAdded;

    [SerializeField] OwnerTag owner;
    [SerializeField] TMP_Text text;
    [SerializeField] int score = 0;

    public OwnerTag Owner { get => owner; set => owner = value; }
    public int Score { get => score; set => score = value; }

    public void RaiseEvent(EventArgs args)
    {
        if (args.GetType() == typeof(CubeControlerEventArgs))
        {
            if (((CubeControlerEventArgs)args).Owner == owner)
                IncreaseScore();
        }
        else if (args.GetType() == typeof(GameStateCapsule))
        {
            OnGameStateChanged(((GameStateCapsule)args).State);
        }
        
    }

    private void OnGameStateChanged(GameState newState)
    {
        switch (newState)
        {
            case GameState.StartScreen:
                ResetScore();
                break;
            case GameState.Loading:
                break;
            case GameState.Launching:
                break;
            case GameState.Play:
                break;
            case GameState.FailCalculating:
                break;
            case GameState.Fail:
                break;
            case GameState.WinCalculating:
                break;
            case GameState.Win:
                break;
            case GameState.Tutorial:
                break;
            default:
                break;
        }
    }

    private void Awake()
    {
        onCubeCollected.Register(this);
        onGameStateChanged.Register(this);
    }

    private void OnDestroy()
    {
        onCubeCollected.Unregister(this);
        onGameStateChanged.Unregister(this);
    }
    private void IncreaseScore()
    {
        score++;
        UpdateText();
    }

    private void UpdateText()
    {
        text.text = score.ToString();
    }

    private void ResetScore()
    {
        score = 0;
        UpdateText();
        RegisterPlayer();
    }

    private void RegisterPlayer()
    {
        onNewPlayerAdded.Invoke(new PlayerRegisteryCapsule(this));
    }
}



public interface IPlayerScoreHolder : IOwnerAdapter
{
    public int Score { get; set; }
}

public class PlayerRegisteryCapsule : EventArgs
{
    public IPlayerScoreHolder Player;
    public PlayerRegisteryCapsule(IPlayerScoreHolder player)
    {
        Player = player;
    }
}