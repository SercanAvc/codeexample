using System;
using UnityEngine;

public abstract class VariableBaseSC<T> : ScriptableObject
{

    public Action<T> OnValueChanged;
    [SerializeField] protected T value;

    public T Value
    {
        get
        {
            return value;
        }

        set
        {
            if (!this.value.Equals(value))
            {
                this.value = value;
                OnValueChanged?.Invoke(this.value);
            }

        }
    }
}
