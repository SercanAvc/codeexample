
using System;
using UnityEngine;
public class GameStateMask : StateMaskBase<GameState>, IScriptableEventListener
{
    [SerializeField] ScriptableEvent OnGameStateChanged;

    public void RaiseEvent(EventArgs args)
    {
        OnStateChanged(((GameStateCapsule)args).State);
    }

    private void Awake()
    {
        OnGameStateChanged.Register(this);
    }
    private void OnDestroy()
    {
        OnGameStateChanged.Unregister(this);

    }
}
