using System;

public interface IScriptableEventListener
{
    void RaiseEvent(EventArgs args);
}

