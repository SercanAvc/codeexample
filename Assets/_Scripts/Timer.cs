using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Timer : MonoBehaviour, IScriptableEventListener
{
    [SerializeField] ScriptableEvent onCounterReachZero;
    [SerializeField] ScriptableEvent onGameStateChanged;
    
    [SerializeField] FloatVariableSC countdownTime;
    [SerializeField] float currentTime; 
    [SerializeField] bool isTimerStarted = false;
    [SerializeField] TMP_Text text;
    int minutes;
    int seconds;
    string timeString;

    private void Awake()
    {
        onGameStateChanged.Register(this);
    }

    private void OnDestroy()
    {
        onGameStateChanged.Unregister(this);

    }
    private void Update()
    {
        if (!isTimerStarted)
            return;

        if (currentTime > 0)
        {
            currentTime -= Time.deltaTime;
            text.text =  ConvertFormat(currentTime);
        }
        else
        {
            StopTimer();
        }
    }
    public void RaiseEvent(EventArgs args)
    {
        switch (((GameStateCapsule)args).State)
        {
            case GameState.StartScreen:
                break;
            case GameState.Loading:
                break;
            case GameState.Launching:
                break;
            case GameState.Play:
                StartTimer();
                break;
            case GameState.FailCalculating:
                break;
            case GameState.Fail:
                break;
            case GameState.WinCalculating:
                break;
            case GameState.Win:
                break;
            case GameState.Tutorial:
                break;
            default:
                break;
        }
    }
    private string ConvertFormat(float time)
    {
        minutes = Mathf.FloorToInt(time / 60);
        seconds = Mathf.FloorToInt(time % 60);

        timeString = string.Format("{0:00}:{1:00}", minutes, seconds);

        return timeString;
    }

    
    public void StartTimer()
    {
        currentTime = countdownTime.Value;
        text.text = ConvertFormat(currentTime);
        isTimerStarted = true;

    }

    public void StopTimer()
    {
        isTimerStarted = false;
        currentTime = 0;
        text.text = "00:00";
        onCounterReachZero.Invoke(new TimerEventCapsule());
    }

    
}
public class TimerEventCapsule : EventArgs
{

}