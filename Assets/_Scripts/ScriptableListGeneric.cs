using UnityEngine;
using System.Collections.Generic;

public class ScriptableListGeneric<T> : ScriptableObject
{
    public List<T> itemList = new List<T>();
    public int Count => itemList.Count;
    public T this[int index]
    {
        get => itemList[index];
        set => itemList[index] = value;
    }
    public void Add(T item)
    {
        itemList.Add(item);
    }

    public void Remove(T item)
    {
        itemList.Remove(item);
    }

}
