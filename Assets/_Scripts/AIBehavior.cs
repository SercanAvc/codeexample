using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class AIBehavior : MonoBehaviour, IScriptableEventListener
{
    [SerializeField] ScriptableEvent onGameStateChanged;

    [SerializeField] ScriptableEvent onActiveCubesOnSceneChanged;
    [SerializeField] ScriptableEvent onTargetPositionChanged;
    [SerializeField] ScriptableEvent onDestinationReaced;

    [SerializeField] Vector3 depositePosition;

    [SerializeField] AIBehaviorState state = AIBehaviorState.None;
    [SerializeField] float reactionTime = 1.0f;
    [SerializeField] int preferedMinTargetObjectDensity = 1;
    [SerializeField] float proximityThreshold = 2f;
    [SerializeField] List<GameObject> activeGameObjects = new List<GameObject>();

    Dictionary<GameObject, int> densityMap = new Dictionary<GameObject, int>();
    int highestValue = 0;
    Vector3 targetPosition;
    Transform highestDensityPositionTransform;
    List<GameObject> selectedObjects = new List<GameObject>();
    GameState gameState;
    private void Awake()
    {
        onActiveCubesOnSceneChanged.Register(this);
        onDestinationReaced.Register(this);
        onGameStateChanged.Register(this);

    }
    private void OnDestroy()
    {
        onActiveCubesOnSceneChanged.Unregister(this);
        onDestinationReaced.Unregister(this);
        onGameStateChanged.Unregister(this);

    }

    public void RaiseEvent(EventArgs args)
    {

        if (args.GetType() == typeof(CubeFountainEventArgs))
        {
            OnActiveObjectsChanged(((CubeFountainEventArgs)args).ActiveObjects);
        }
        else if (args.GetType() == typeof(CollectorControllerNavMeshEventArgs))
        {
            OnDestinationReached();
        }
        else if (args.GetType() == typeof(GameStateCapsule))
        {
            OnGameStateChanged(((GameStateCapsule)args).State);
        }
    }
    private void OnGameStateChanged(GameState newState)
    {
        gameState = newState;

        switch (newState)
        {
            case GameState.StartScreen:
                StopAllCoroutines();
                break;
            case GameState.Loading:
                break;
            case GameState.Launching:
                break;
            case GameState.Play:
                StartCoroutine(LookForTarget());
                break;
            case GameState.FailCalculating:
                StopAllCoroutines();
                break;
            case GameState.Fail:
                break;
            case GameState.WinCalculating:
                StopAllCoroutines();
                break;
            case GameState.Win:
                break;
            case GameState.Tutorial:
                break;
            default:
                break;
        }
    }
    private void OnDestinationReached()
    {
        if (gameState != GameState.Play)
            return;

        StopAllCoroutines();
        
        switch (state)
        {
            case AIBehaviorState.None:
                break;
            case AIBehaviorState.Searching:
                break;
            case AIBehaviorState.GoingToTarget:
                ReturnDeposite();
                break;
            case AIBehaviorState.GoingToDeposite:
                StartCoroutine(LookForTarget());
                break;
            default:
                break;
        }

    }
    private void ReturnDeposite()
    {
        onTargetPositionChanged.Invoke(new InputSystemEventArgs(depositePosition));
        state = AIBehaviorState.GoingToDeposite;
    }

    private void OnActiveObjectsChanged(List<GameObject> newList)
    {
        activeGameObjects.Clear();
        
        for (int i = 0; i < newList.Count; i++)
        {
            IOwnerAdapter ownerAdapter = newList[i].GetComponent<IOwnerAdapter>();

            if (ownerAdapter.Owner != OwnerTag.None)
                continue;

            activeGameObjects.Add(newList[i]);
        }
    }

    private IEnumerator LookForTarget()
    {
        while (true)
        {
            yield return new WaitForSeconds(reactionTime);

            CalculateObjectDensity(activeGameObjects);
            selectedObjects.Clear();
            highestValue = 0;
            highestDensityPositionTransform = null;

            foreach (KeyValuePair<GameObject, int> pair in densityMap)
            {
                if (pair.Value >= preferedMinTargetObjectDensity)
                {
                    selectedObjects.Add(pair.Key);
                }

                if (pair.Value > highestValue)
                {
                    highestValue = pair.Value;
                    highestDensityPositionTransform = pair.Key.transform;
                }
            }

            if (selectedObjects.Count <= 0)
            {
                if (highestDensityPositionTransform != null)
                {
                    SetTartgetPosition(highestDensityPositionTransform.position);
                }
            }
            else
            {
                SetTartgetPosition(selectedObjects[Random.Range(0, selectedObjects.Count)].transform.position);
            }

            if (highestDensityPositionTransform == null && selectedObjects.Count <= 0)
            {
                state = AIBehaviorState.Searching;
            }
            else
            {
                state = AIBehaviorState.GoingToTarget;
            }
        }


    }
    private void SetTartgetPosition(Vector3 target)
    {
        targetPosition = target;
        onTargetPositionChanged.Invoke(new InputSystemEventArgs(targetPosition));
    }

    private void CalculateObjectDensity(List<GameObject> objectList)
    {
        List<GameObject> objects = new List<GameObject>(objectList);

        densityMap.Clear();

        for (int i = 0; i < objects.Count; i++)
        {
            int neighborCount = 0;

            for (int j = 0; j < objects.Count; j++)
            {
                if (i != j && Vector3.Distance(objects[i].transform.position, objects[j].transform.position) <= proximityThreshold)
                {
                    neighborCount++;
                }
            }

            densityMap[objects[i]] = neighborCount;
        }
    }


}
public enum AIBehaviorState
{
    None = 0,
    Searching,
    GoingToTarget,
    GoingToDeposite
}