using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputSystem : MonoBehaviour, IDragHandler
{
    [SerializeField] ScriptableEvent OnFingerPositionChanged;
    [SerializeField] Vector2 currentPosition;
    [SerializeField] Vector3 direction;
    
    public void OnDrag(PointerEventData eventData)
    {
        direction = currentPosition - eventData.position;
        
        OnFingerPositionChanged.Invoke(new InputSystemEventArgs( direction));
        currentPosition = eventData.position;
    }
}
public class InputSystemEventArgs : EventArgs
{
    public Vector3 Direction;

    public InputSystemEventArgs(Vector3 direction)
    {
        Direction = direction;
    }
}