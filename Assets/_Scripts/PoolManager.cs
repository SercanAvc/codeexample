using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour
{
    public static PoolManager Instance { get; private set; }

    [SerializeField] GameObject objectPrefab;
    [SerializeField] int initialPoolSize = 10;

    Queue<GameObject> objectPool;
    IPoolObject tempIPoolObject;
    private void Awake()
    {
        if (objectPrefab != null && !objectPrefab.TryGetComponent(out tempIPoolObject))
            throw new Exception("Game Object in the pool needs to implement IPoolObject interface.");

        if (Instance == null)
            Instance = this;
        else
        {
            Debug.LogWarning("Multiple instances of ObjectPoolManager found!");
            Destroy(gameObject);
            return;
        }

        CreatePool();
    }

    
    private void CreatePool()
    {
        objectPool = new Queue<GameObject>();

        for (int i = 0; i < initialPoolSize; i++)
        {
            GameObject obj = InstantiateObject();

            if (obj.TryGetComponent(out tempIPoolObject))
                tempIPoolObject.OnCreate();

            obj.SetActive(false);
            objectPool.Enqueue(obj);
        }
    }

    public GameObject GetObjectFromPool(bool isActive = false)
    {
        if (objectPool.Count == 0)
        {
            GameObject obj = InstantiateObject();
            objectPool.Enqueue(obj);
        }

        GameObject pooledObject = objectPool.Dequeue();
        pooledObject.SetActive(isActive);

        return pooledObject;
    }

    public void ReturnObjectToPool(GameObject obj)
    {
        if(obj.TryGetComponent(out tempIPoolObject))
            tempIPoolObject.ClearForRelease();

        obj.SetActive(false);
        objectPool.Enqueue(obj);
    }

    private GameObject InstantiateObject()
    {
        GameObject obj = Instantiate(objectPrefab);
        
        return obj;
    }

    private void OnValidate()
    {
        if (objectPrefab != null && !objectPrefab.TryGetComponent(out tempIPoolObject))
        {
            Debug.LogError("Game Object in the pool needs to implement IPoolObject interface.");
        }
    }
}

public interface IPoolObject
{
    void ClearForRelease();
    void OnCreate();
}