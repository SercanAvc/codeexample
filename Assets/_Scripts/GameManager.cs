using System;
using UnityEngine;

public class GameManager : MonoBehaviour, IScriptableEventListener
{
    private static GameManager _gameStateManager;
    [SerializeField] private GameState currentState;

    public ScriptableEvent OnGameStateChanged;
    public ScriptableEvent OnLevelCompleted;
    public ScriptableEvent NextLevelCall;

    private void Awake()
    {
        if (_gameStateManager == null)
            _gameStateManager = this;
        else
            Destroy(gameObject);

        OnLevelCompleted.Register(this);
        NextLevelCall.Register(this);
    }

    public void RaiseEvent(EventArgs args)
    {
        if (args.GetType() == typeof(LevelHandlerArgs))
        {
            if (((LevelHandlerArgs)args).Winner == OwnerTag.Player)
            {
                SetState(GameState.WinCalculating);
            }
            else
            {
                SetState(GameState.FailCalculating);
            }
        }
        else if (args.GetType() == typeof(LevelHandlerUIBridgeEventArgs))
        {
            SetState(GameState.StartScreen);
        }

    }

    private void Start()
    {
        OnGameStateChanged.Invoke(new GameStateCapsule(currentState));
    }
    public static GameState GetState()
    {

        return _gameStateManager.currentState;
    }

    public static void SetState(GameState gameState)
    {
        _gameStateManager.currentState = gameState;
        _gameStateManager.OnGameStateChanged.Invoke(new GameStateCapsule(gameState));
    }


}

[Serializable]
public enum GameState
{
    StartScreen = 0, // Very first State  (Tap To start)
    Loading = 1, // loadingSplash
    Launching = 2, //Game play start to Controls are available period (character wake up, plane takes off ect.)
    Play = 3, // controls are available and game has started
    FailCalculating = 4, // Game ended player lose , waiting for calculation or adscene or any other penalty to end (untill retry enabled)
    Fail = 5, // Game ended and player lose state untill retry pressed
    WinCalculating = 6, //  Game ended player won , waiting for calculation or adscene or anyother penalty to end (untill Next Level enabled)
    Win = 7, // player won state (untill the retry pressed)
    Tutorial = 8, // Tutorial info
}

public class GameStateCapsule : EventArgs
{
    public GameState State;

    public GameStateCapsule(GameState state)
    {
        State = state;
    }
}