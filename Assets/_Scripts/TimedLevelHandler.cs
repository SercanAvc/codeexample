using System;
using System.Collections.Generic;
using UnityEngine;

public class TimedLevelHandler : MonoBehaviour, IScriptableEventListener
{
    [SerializeField] ScriptableEvent onLevelCompleted;
    [SerializeField] ScriptableEvent onTimerReachZero;
    [SerializeField] ScriptableEvent onNewPlayerAdded;
    [SerializeField] List<IPlayerScoreHolder> players = new List<IPlayerScoreHolder>();
    
    int highestValue;
    IPlayerScoreHolder winner;
    
    private void Awake()
    {
        onTimerReachZero.Register(this);
        onNewPlayerAdded.Register(this);

    }

    private void OnDestroy()
    {
        onTimerReachZero.Unregister(this);
        onNewPlayerAdded.Unregister(this);

    }

    public void RaiseEvent(EventArgs args)
    {
        if (args.GetType() == typeof(TimerEventCapsule))
        {
            DecideWinner();
        }
        else if (args.GetType() == typeof(PlayerRegisteryCapsule))
        {
            AddPlayer(((PlayerRegisteryCapsule)args).Player);
        }
        
    }
    
    private void AddPlayer(IPlayerScoreHolder newPlayer)
    {
        if (!players.Contains(newPlayer))
        {
            players.Add(newPlayer);
        }
    }
    
    private void DecideWinner()
    {

        for (int i = 0; i < players.Count; i++)
        {
            if (players[i].Score >= highestValue)
            {
                highestValue = players[i].Score;
                winner = players[i];
            }
        }
        

        onLevelCompleted.Invoke(new LevelHandlerArgs(winner.Owner));
    }
}


