using System;
using UnityEngine;
using UnityEngine.AI;

public class CollectorControllerNavMesh : ControllerBase
{
    [SerializeField] NavMeshAgent agent;
    [SerializeField] ScriptableEvent OnDestinationReached;
    [SerializeField] bool haveTarget = false;
    Vector3 targetPos;
    
    protected override void Move(Vector3 dir)
    {
        dir.y = transform.position.y;
        agent.SetDestination(dir);
        targetPos = dir;
        haveTarget = true;

    }
    
    private void LateUpdate()
    {
        CheckDistance();
    }

    private void CheckDistance()
    {
        if (haveTarget && Vector3.Distance(transform.position, targetPos) <= agent.stoppingDistance)
        {
            haveTarget = false;
            OnDestinationReached.Invoke(new CollectorControllerNavMeshEventArgs());
        }
    }

    public override void Reset()
    {
        agent.enabled = false;
        base.Reset();
        agent.enabled = true;

        haveTarget = false;
    }
    protected override void OnGameStateChanged(GameState state)
    {
        switch (state)
        {
            case GameState.StartScreen:
                Reset();
                break;
            case GameState.Loading:
                break;
            case GameState.Launching:
                break;
            case GameState.Play:
                agent.enabled = true;

                break;
            case GameState.FailCalculating:
                agent.enabled = false;
                break;
            case GameState.Fail:
                break;
            case GameState.WinCalculating:
                agent.enabled = false;
                break;
            case GameState.Win:
                break;
            case GameState.Tutorial:
                break;
            default:
                break;
        }
    }


}
public class CollectorControllerNavMeshEventArgs : EventArgs
{

}
