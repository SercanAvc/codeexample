using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableList", menuName = "ScriptableObjects/GameObject List")]
public class ScriptableGameObjectList : ScriptableListGeneric<GameObject>
{
    
}