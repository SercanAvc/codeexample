using UnityEngine;

public class DepositeController : MonoBehaviour, IOwnerAdapter
{
    [SerializeField] float pullForce = 10f;
    [SerializeField] float forceDuration = 1f;
    [SerializeField] OwnerTag owner;
    public OwnerTag Owner { get => owner; set => owner = value; }

    private void OnTriggerEnter(Collider other)
    {
        SetOwner(other);
        Pull(other);
    }

    private void SetOwner(Collider other)
    {
        IOwnerAdapter adapter = other.GetComponentInParent<IOwnerAdapter>();

        if (adapter != null)
            adapter.Owner = Owner;
    }

    private void Pull(Collider other)
    {
        IDepositePullHandler handler = other.GetComponentInParent<IDepositePullHandler>();

        if (handler != null)
            handler.Pull(transform, pullForce, forceDuration);
    }
}
public interface IDepositePullHandler
{
    void Pull(Transform targetTransform, float force, float duration);
}