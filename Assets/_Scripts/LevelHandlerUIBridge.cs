using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelHandlerUIBridge : MonoBehaviour
{
    [SerializeField] ScriptableEvent nextLevelEvent;
    
    public void NextLevel()
    {
        nextLevelEvent.Invoke(new LevelHandlerUIBridgeEventArgs());
    }
    
}

public class LevelHandlerUIBridgeEventArgs : EventArgs
{

}
