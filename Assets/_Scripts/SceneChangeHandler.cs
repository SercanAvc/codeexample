using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChangeHandler : MonoBehaviour
{
    public void MainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void PlayPartOne()
    {
        SceneManager.LoadScene(1);
    }

    public void PlayPartTwo()
    {
        SceneManager.LoadScene(2);
    }

    public void PlayPartThree()
    {
        SceneManager.LoadScene(3);
    }
}
