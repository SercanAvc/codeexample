using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelHandler : MonoBehaviour, IScriptableEventListener
{
    [SerializeField] ScriptableEvent onLevelCompleted;
    [SerializeField] ScriptableEvent onCubesGenerated;
    [SerializeField] ScriptableEvent onCubeCollected;

    [SerializeField] ScriptableEvent onCubesCleared;
    [SerializeField] List<GameObject> activeCubes = new List<GameObject>();
    [SerializeField] int activeCubeCount;
    private void Awake()
    {
        onCubesGenerated.Register(this);
        onCubesCleared.Register(this);

        onCubeCollected.Register(this);
    }

    private void OnDestroy()
    {
        onCubesGenerated.Unregister(this);
        onCubesCleared.Unregister(this);
        onCubeCollected.Unregister(this);
    }

    public void RaiseEvent(EventArgs args)
    {
        if(args.GetType() == typeof(CubeGeneratorEventCapsule))
        {

            if (((CubeGeneratorEventCapsule)args).IsCreated)
            {
                OnLevelCreated(((CubeGeneratorEventCapsule)args).Cubes);
            }
            else
            {
                OnLevelRemoved();
            }
        }
        else if (args.GetType() == typeof(CubeControlerEventArgs))
        {
            OnCollectedCube();
        }
    }
    private void OnLevelCreated(List<GameObject> cubesOnScene)
    {
        if (activeCubes.Count > 0)
            activeCubes.Clear();

        activeCubes.AddRange(cubesOnScene);
        activeCubeCount = cubesOnScene.Count;
    }

    private void OnCollectedCube()
    {
        activeCubeCount--;

        if (activeCubeCount <= 0)
            onLevelCompleted.Invoke(new LevelHandlerArgs(OwnerTag.Player));
    }

    private void OnLevelRemoved()
    {
        if (activeCubes.Count > 0)
            activeCubes.Clear();

        activeCubeCount = 0;
    }

    
}

public class LevelHandlerArgs : EventArgs
{
    public OwnerTag Winner;

    public LevelHandlerArgs(OwnerTag winner)
    {
        Winner = winner;
    }
}