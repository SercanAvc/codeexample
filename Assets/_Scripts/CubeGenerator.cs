using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


public class CubeGenerator : MonoBehaviour, IScriptableEventListener
{
    [SerializeField] ScriptableEvent gameStateEvent;
    [SerializeField] ScriptableEvent onCubesGenerated;
    [SerializeField] ScriptableEvent onCubesCleared;

    [SerializeField] GameObject cubePrefab;
    [SerializeField] ScriptableCubeShape currentShape;
    [SerializeField] ScriptableCubeShape[] shapeList;
    [SerializeField] List<GameObject> cubes = new List<GameObject>();
    [SerializeField] int currentShapeIndex = 0;

    private void Awake()
    {
        gameStateEvent.Register(this);
        currentShape = shapeList[currentShapeIndex];
    }
    private void OnDestroy()
    {
        gameStateEvent.Unregister(this);
    }

    public void RaiseEvent(EventArgs args)
    {
        switch (((GameStateCapsule)args).State)
        {
            case GameState.StartScreen:
                GenerateCubesFromImage();
                break;
            case GameState.Loading:
                break;
            case GameState.Launching:
                break;
            case GameState.Play:
                break;
            case GameState.FailCalculating:
                break;
            case GameState.Fail:
                break;
            case GameState.WinCalculating:
                break;
            case GameState.Win:
                break;
            case GameState.Tutorial:
                break;
            default:
                break;
        }
    }
    private void Start()
    {
        if (cubes.Count > 0)
            onCubesGenerated.Invoke(new CubeGeneratorEventCapsule(true, cubes));

    }
    
    public void NextShape()
    {
        currentShapeIndex++;
        currentShape = shapeList[currentShapeIndex % shapeList.Length];
    }
    public void GenerateCubesFromImage()
    {
        ClearCubes();
        
        for (int i = 0; i < currentShape.Pairs.Count; i++)
        {
            GameObject go = PoolManager.Instance.GetObjectFromPool();
            go.transform.parent = transform;
            go.transform.localPosition = currentShape.Pairs[i].Position;
            
            Renderer cubeRenderer = go.GetComponentInChildren<Renderer>();
            cubeRenderer.material.color = currentShape.Pairs[i].Color;
            go.SetActive(true);
            cubes.Add(go);
        }

        onCubesGenerated.Invoke(new CubeGeneratorEventCapsule(true, cubes));
    }
    public void ClearCubes()
    {
        if (cubes.Count <= 0)
            return;

        for (int i = cubes.Count - 1; i >= 0; i--)
        {
            GameObject go = cubes[i];
            cubes.Remove(go);
#if UNITY_EDITOR
            DestroyImmediate(go);
#else
            PoolManager.Instance.ReturnObjectToPool(go);
#endif
        }
        NextShape();

        onCubesCleared.Invoke(new CubeGeneratorEventCapsule(false, cubes));
    }

    
}
#if UNITY_EDITOR
[CustomEditor(typeof(CubeGenerator))]
public class CubeGeneratorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        CubeGenerator generator = (CubeGenerator)target;

        if (GUILayout.Button("Generate"))
        {
            generator.GenerateCubesFromImage();
        }

        if (GUILayout.Button("Clear"))
        {
            generator.ClearCubes();
        }

    }
}
#endif
public class CubeGeneratorEventCapsule : EventArgs
{
    public bool IsCreated = false;
    public List<GameObject> Cubes = new List<GameObject>();

    public CubeGeneratorEventCapsule(bool isCreated, List<GameObject> cubes)
    {
        IsCreated = isCreated;
        Cubes = cubes;
    }
}