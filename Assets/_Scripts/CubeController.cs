using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeController : MonoBehaviour, IPoolObject, IDepositePullHandler, IOwnerAdapter
{
    [SerializeField] ScriptableEvent OnCubeCollected;
    [SerializeField] Rigidbody rb;
    [SerializeField] GameObject activeCollider;
    [SerializeField] GameObject passiveCollider;
    [SerializeField] bool isCollected = false;
    [SerializeField] OwnerTag owner = OwnerTag.None;
    [SerializeField] GameObject[] ownerColliders;
    float pullStartTime = 0;
    Vector3 directionToCenter;
    CollectorTag collectorTag;
    public OwnerTag Owner { get => owner; set => owner = value; }

    #region IPoolObject
    public void ClearForRelease()
    {
        activeCollider.SetActive(true);
        passiveCollider.SetActive(false);
        isCollected = false;
        rb.isKinematic = false;
    }

    public void OnCreate()
    {
        activeCollider.SetActive(true);
        passiveCollider.SetActive(false);
    }
    #endregion
    public void Collect()
    {
        if (isCollected)
            return;

        activeCollider.SetActive(false);
        passiveCollider.SetActive(true);
        isCollected = true;
        OnCubeCollected.Invoke(new CubeControlerEventArgs(owner));
    }

    private void OnTriggerEnter(Collider other)
    {
        collectorTag = other.GetComponentInParent<CollectorTag>();
        if(collectorTag != null)
        {
            SwitchOwner(collectorTag.Owner);
        }
    }

    private void SwitchOwner(OwnerTag newOwner)
    {
        if (isCollected)
            return;

        owner = newOwner;

        for (int i = 0; i < ownerColliders.Length; i++)
        {
            ownerColliders[i].SetActive(i == (int)newOwner);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        SwitchOwner(OwnerTag.None);
    }


    #region IDepositeHandler
    public void Pull(Transform targetTransform, float force, float duration)
    {
        if (isCollected)
            return;

        Collect();
        pullStartTime = Time.time;
        StartCoroutine(PullCoroutine(targetTransform, force, duration));
    }

    private IEnumerator PullCoroutine(Transform targetTransform, float pullForce, float duration)
    {
        activeCollider.SetActive(false);
        passiveCollider.SetActive(true);
        
        while (pullStartTime + duration >= Time.time)
        {
            if (rb != null && Time.frameCount % 5 == 0)
            {
                directionToCenter = targetTransform.position - transform.position;
                rb.AddForce(directionToCenter.normalized * pullForce, ForceMode.Force);
            }

            yield return null;
        }

        rb.isKinematic = true;
    }

    #endregion
}



public class CubeControlerEventArgs : EventArgs
{
    public OwnerTag Owner = OwnerTag.None;

    public CubeControlerEventArgs(OwnerTag owner)
    {
        Owner = owner;
    }
}
