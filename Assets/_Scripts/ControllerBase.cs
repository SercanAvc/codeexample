using System;
using UnityEngine;

public abstract class ControllerBase : MonoBehaviour, IScriptableEventListener, IResetable
{
    [SerializeField] protected ScriptableEvent onGameStateChanged;
    [SerializeField] protected ScriptableEvent onFingerPositionChanged;

    protected Vector3 startPosition;
    protected Quaternion startRotation;
    protected virtual void Awake()
    {
        startPosition = transform.position;
        startRotation = transform.rotation;
        onGameStateChanged.Register(this);
        onFingerPositionChanged.Register(this);
    }
    public virtual void RaiseEvent(EventArgs args)
    {
        if (args.GetType() == typeof(InputSystemEventArgs))
        {
            Move(((InputSystemEventArgs)args).Direction);
        }
        else if (args.GetType() == typeof(GameStateCapsule))
        {
            OnGameStateChanged(((GameStateCapsule)args).State);
        }
    }

    protected abstract void Move(Vector3 dir);
    protected abstract void OnGameStateChanged(GameState state);


    protected virtual void OnDestroy()
    {
        onGameStateChanged.Unregister(this);
        onFingerPositionChanged.Unregister(this);

    }

    public virtual void Reset()
    {
        transform.SetPositionAndRotation(startPosition, startRotation);
    }

}

public interface IResetable
{
    void Reset();
}
