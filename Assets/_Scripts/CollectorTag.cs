using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectorTag : MonoBehaviour
{
    public OwnerTag Owner = OwnerTag.None;
}
public interface IOwnerAdapter
{
    OwnerTag Owner { get; set; }
}
public enum OwnerTag
{
    None,
    Player,
    Rival
}