using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class StateMaskBase<T> : MonoBehaviour
{
    [SerializeField] protected List<T> mask;
    
    protected virtual void OnStateChanged(T state)
    {
        if (mask.Contains(state))
            gameObject.SetActive(true);
        else
            gameObject.SetActive(false);
    }
}
