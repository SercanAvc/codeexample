using System;
using System.Collections.Generic;
using UnityEngine;

public class CubeFountain : MonoBehaviour, IScriptableEventListener
{
    [SerializeField] ScriptableEvent OnGameStateChanged;
    [SerializeField] ScriptableEvent OnActiveCubesOnSceneChanged;

    [SerializeField] float spawnRate = 1f;
    [SerializeField] int maxSpawnCount = 10;
    [SerializeField] float forceMagnitude = 10f;
    [SerializeField] bool isActive = false;
    [SerializeField] List<GameObject> activeCubesOnScene = new List<GameObject>();
    float spawnTimer;
    private void Awake()
    {
        OnGameStateChanged.Register(this);
    }
    private void OnDestroy()
    {
        OnGameStateChanged.Unregister(this);
    }
    public void RaiseEvent(EventArgs args)
    {
        switch (((GameStateCapsule)args).State)
        {
            case GameState.StartScreen:
                ResetSpawner();
                break;
            case GameState.Loading:
                break;
            case GameState.Launching:
                break;
            case GameState.Play:
                ResetSpawner();
                StartSpawner();
                break;
            case GameState.FailCalculating:
                break;
            case GameState.Fail:
                break;
            case GameState.WinCalculating:
                StopTimer();
                break;
            case GameState.Win:
                break;
            case GameState.Tutorial:
                break;
            default:
                break;
        }
    }
    private void StopTimer()
    {
        isActive = false;
    }
    private void ResetSpawner()
    {
        isActive = false;
        spawnTimer = 0f;
        for (int i = activeCubesOnScene.Count - 1; i >= 0; i--)
        {
            PoolManager.Instance.ReturnObjectToPool(activeCubesOnScene[i]);
            activeCubesOnScene.Remove(activeCubesOnScene[i]);
        }
    }
    private void StartSpawner()
    {
        isActive = true;
    }
    private void Update()
    {
        if (!isActive)
            return;

        if (activeCubesOnScene.Count < maxSpawnCount)
        {
            spawnTimer += Time.deltaTime;

            if (spawnTimer >= spawnRate)
            {
                SpawnObject();
                spawnTimer = 0f;
            }
        }
    }

    private void SpawnObject()
    {
        GameObject spawnedObject = PoolManager.Instance.GetObjectFromPool();

        spawnedObject.transform.position = transform.position;
        Rigidbody rb = spawnedObject.GetComponent<Rigidbody>();

        if (rb != null)
        {
            // Calculate the angle for the circular motion
            float angle = (2f * Mathf.PI / maxSpawnCount) * activeCubesOnScene.Count;

            // Calculate the direction of the force using the angle
            Vector3 forceDirection = new Vector3(Mathf.Cos(angle), 1f, Mathf.Sin(angle));

            // Apply the force to the object
            rb.AddForce(forceDirection * forceMagnitude, ForceMode.Impulse);
        }

        spawnedObject.SetActive(true);
        activeCubesOnScene.Add(spawnedObject);
        OnActiveCubesOnSceneChanged.Invoke(new CubeFountainEventArgs(activeCubesOnScene));
        if (activeCubesOnScene.Count >= maxSpawnCount)
        {
            isActive = false;
        }
    }


}

public class CubeFountainEventArgs : EventArgs
{
    public List<GameObject> ActiveObjects = new List<GameObject>();

    public CubeFountainEventArgs(List<GameObject> activeObjects)
    {
        ActiveObjects = activeObjects;
    }
}
