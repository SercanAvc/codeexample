using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectorController : MonoBehaviour
{
    InputSystem inputSystem;

    [SerializeField] float moveSpeed = 1f;
    [SerializeField] float rotationSpeed = 1f;
    [SerializeField] float damping = 1f;
    [SerializeField] float maxInputSpeed = 20f;

    [SerializeField] Vector3 directionVector = Vector3.zero;
    [SerializeField] Vector3 targetPosition = Vector3.zero;
    Vector3 currentVelocity;
    
    void Start()
    {
        inputSystem = FindObjectOfType<InputSystem>();

        if (inputSystem == null)
            Debug.Log("null input");

        //inputSystem.OnFingerPositionChanged += Move;

        directionVector.y = transform.position.y;
    }

    private void OnDestroy()
    {
        //inputSystem.OnFingerPositionChanged -= Move;
    }

    private void FixedUpdate()
    {
        if (Vector3.Distance(transform.position, targetPosition) < 0.01f)
            return;

        
        transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref currentVelocity, damping, moveSpeed, Time.deltaTime);
    }

    private void Move(Vector3 dir, float speed)
    {
        directionVector.x = dir.x;
        directionVector.z = dir.y;
        directionVector *= -1f;
        directionVector.Normalize();

        targetPosition = transform.position + (Mathf.Clamp(speed, 0f, maxInputSpeed) * moveSpeed * Time.deltaTime * directionVector);

        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(directionVector), Time.deltaTime * rotationSpeed);
    }
}
