using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleController : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        IResetable resetable = other.GetComponentInParent<IResetable>();

        resetable.Reset();
    }
}
