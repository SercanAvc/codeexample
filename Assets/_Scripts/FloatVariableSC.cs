using UnityEngine;

[CreateAssetMenu(fileName = "New Float Scriptable Variable", menuName = "ScriptableObjects/Variables/Float")]
public class FloatVariableSC : VariableBaseSC<float>
{
    
}

