using UnityEngine;

public class CollectorControllerPhysics : ControllerBase
{
    [SerializeField] FloatVariableSC moveSpeed;
    [SerializeField] FloatVariableSC rotationSpeed;
    [SerializeField] FloatVariableSC damping;
    [SerializeField] FloatVariableSC maxInputSpeed;
    [SerializeField] FloatVariableSC gravity;

    Vector3 directionVector = Vector3.zero;
    Rigidbody rb;
    
    protected override void Awake()
    {
        base.Awake();

        rb = GetComponent<Rigidbody>();
    }

    protected override void OnGameStateChanged(GameState state)
    {
        switch (state)
        {
            case GameState.StartScreen:
                Reset();
                break;
            case GameState.Loading:
                break;
            case GameState.Launching:
                break;
            case GameState.Play:
                break;
            case GameState.FailCalculating:
                break;
            case GameState.Fail:
                break;
            case GameState.WinCalculating:
                break;
            case GameState.Win:
                break;
            case GameState.Tutorial:
                break;
            default:
                break;
        }
    }

    
    private void FixedUpdate()
    {
        ApplyArtificialGravity();
    }

    
    private void ApplyArtificialGravity()
    {
        rb.velocity += gravity.Value * Time.deltaTime * Vector3.up;
    }

    public override void Reset()
    {
        base.Reset();

        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
    }

    protected override void Move(Vector3 dir)
    {
        directionVector.x = dir.x;
        directionVector.z = dir.y;
        directionVector *= -1f;

        rb.velocity += moveSpeed.Value * Time.deltaTime * directionVector;

        if (rb.velocity.magnitude > maxInputSpeed.Value)
            rb.velocity = rb.velocity.normalized * maxInputSpeed.Value;

        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(directionVector), Time.deltaTime * rotationSpeed.Value);

        directionVector = Vector3.zero;
    }


}
